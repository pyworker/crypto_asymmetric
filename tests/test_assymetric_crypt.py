import unittest
from os.path import exists, abspath
from os import remove
from generate_keys import genkey
from assymetric_crypt import AssymetricCrypt


DEST = 'mike'
DESTPUB = './keys/mikekey.public'
DESTPRIV = './keys/mikekey.private'

RAW = 'secret message'

class TestAssymetricCrypt(unittest.TestCase):

    def test_get_users(self):
        """
        testing correct work
        Raises: None
        """

        # prepare enviroment before test

        ac = AssymetricCrypt()

        for item in [DESTPRIV, DESTPUB]:
            try:
                remove(item)
                print('Delete key at: %s' % abspath(item))
            except OSError as os_err:
                pass

        genkey(destination=DEST)
        enc_msg = ac.encrypt(source=RAW, path_pubkey=DESTPUB)
        ans = ac.decrypt(source=enc_msg, path_privkey=DESTPRIV)
        should_ans = RAW

        self.assertEqual(ans, should_ans)

        # clean enviroment after test
        for item in [DESTPRIV, DESTPUB]:
            try:
                remove(item)
                print('Delete key at: %s' % abspath(item))
            except OSError as os_err:
                pass


if __name__ == '__main__':
    unittest.main()