"""
module for asymmetric RSA encryption
"""

import base64
from Crypto import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP


class AssymetricCrypt(object):
    """docstring for AssymetricCrypt"""
    def __init__(self):
        pass

    def encrypt(self, source, path_pubkey):
        """
            Encrypt msg using our public key.
            Arguments:
                source: str, The plain text to encrypt
                path_pubkey: str, The public key to be used for encryption
            Return:
                The encrypted text using the public key
        """

        with open(path_pubkey, 'r') as pub_file:
            pub_key = RSA.importKey(pub_file.read())

        cipher = PKCS1_OAEP.new(pub_key)

        encrypted_text = cipher.encrypt(source)
        encoded_encrypted_msg = base64.b64encode(encrypted_text)
        return encoded_encrypted_msg

    def decrypt(self, source, path_privkey):
        """ Decrypt the supplied string using our private key.
        Arguments:
            source: str, the encrypted message
            path_privkey: str, path to the RSA private key to decrypt
        Return:
            the decrypted text
        """
        with open(path_privkey, "r") as pvt_file:
            pvt_key = RSA.importKey(pvt_file.read())
        
        cipher = PKCS1_OAEP.new(pvt_key)

        source = base64.b64decode(source)
        decrypted_text = cipher.decrypt(source)
        return decrypted_text
