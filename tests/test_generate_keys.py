import unittest
from os import remove
from os.path import exists, abspath
from generate_keys import genkey

DEST = 'mike'
DESTPUB = 'mikekey.public'
DESTPRIV = 'mikekey.private'
PATH_PREFIX = './keys/'

class TestGenerateKeys(unittest.TestCase):

    def test_genkey(self):
        """
        test currect work 
        """
        # prepare enviroment before test

        for item in [DESTPRIV, DESTPUB]:
            try:
                remove(item)
                print('Delete key at: %s' % abspath(item))
            except OSError as os_err:
                pass

        genkey(destination=DEST)
        r1 = exists(''.join([PATH_PREFIX, DESTPRIV]))
        r2 = exists(''.join([PATH_PREFIX, DESTPUB]))
        ans = (r1 and r2)
        should_ans = True

        self.assertEqual(ans, should_ans)

        # clean enviroment after test
        for item in [DESTPRIV, DESTPUB]:
            try:
                remove(item)
                print('Delete key at: %s' % abspath(item))
            except OSError as os_err:
                pass
